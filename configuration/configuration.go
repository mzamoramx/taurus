package configuration

import (
	"encoding/json"
	"log"
	"os"
	"time"

	"fmt"

	"gopkg.in/mgo.v2"
)

var (
	dbSession *mgo.Session
)

// Configuration holds the main information used along the project
type Configuration struct {
	Server           string
	Port             string
	User             string
	Password         string
	EmailLog         string       `json:"email_log"`
	EmailCredentials *Credentials `json:"email_credentials"`
	Databases        map[string]string
	Env              *Env `json:"env"`
}

// Credentials holds information about the emails' host.
type Credentials struct {
	Host     string
	Port     int
	UserName string
	Password string
}

// Env holds information about environment site
type Env struct {
	Dev bool `json:"dev"`
}

// GetConfiguration gets the project's configuration
func GetConfiguration() Configuration {
	var c Configuration
	file, err := os.Open("./config.json")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	err = json.NewDecoder(file).Decode(&c)

	if err != nil {
		log.Fatal(err)
	}
	return c
}

// GetConnection gets the session to the db
func GetConnection() (*mgo.Session, error) {
	if dbSession == nil {
		c := GetConfiguration()

		// Connect to our local mongo
		mongoDBDialInfo := &mgo.DialInfo{
			Addrs:    []string{c.Server},
			Timeout:  3 * time.Second,
			Database: c.Databases["db_main"],
			Username: c.User,
			Password: c.Password,
		}

		var err error
		dbSession, err = mgo.DialWithInfo(mongoDBDialInfo)
		if err != nil {
			// Check for connection error, is mongo running?
			fmt.Println(err)
			return nil, err
		}
	}
	return dbSession.Copy(), nil
}

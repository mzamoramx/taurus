package mail

import (

	/* native */

	"io"
	"log"

	/* addons */
	"gopkg.in/gomail.v2"

	/* layers */

	"bitbucket.org/neuboxspacelab/taurus/configuration"
	"bitbucket.org/neuboxspacelab/taurus/views"
)

var c configuration.Configuration

func init() {
	c = configuration.GetConfiguration()
}

type (
	// MailData Check the Email Templates in tpl folder to know how these work
	MailData struct {
		Name   string
		Email  string
		Link   string
		IP     string
		ToName string // Email of the park contact receiver
	}
)

// SendMail notifies the user about an action in the site (resetpwd, resetpwd-finish and signup)
func (data *MailData) SendMail(toEmail, category string) {
	m := gomail.NewMessage()
	m.SetAddressHeader("From", "noreply@sitemaker.mx", "SiteMaker")
	m.SetAddressHeader("To", toEmail, toEmail)

	switch category {
	case "resetpwd":
		m.SetHeader("Subject", "Cambio de contraseña")
		m.AddAlternativeWriter("text/html", func(w io.Writer) error {
			return views.T("mail-resetpwd.html").Execute(w, data)
		})
	case "resetpwd-finish":
		m.SetHeader("Subject", "Tu contraseña ha sido cambiada")
		m.AddAlternativeWriter("text/html", func(w io.Writer) error {
			return views.T("mail-resetpwd-finish.html").Execute(w, data)
		})
	case "signup":
		m.SetHeader("Subject", "Bienvenido abordo")
		m.AddAlternativeWriter("text/html", func(w io.Writer) error {
			return views.T("mail-signup-confirm.html").Execute(w, data)
		})
	}

	log.Println("Mandando mail a: ", toEmail, "(", category, ")")

	/*d := gomail.NewDialer(
		c.EmailCredentials.Host,
		c.EmailCredentials.Port,
		c.EmailCredentials.UserName,
		c.EmailCredentials.Password,
	)*/

	d := gomail.NewDialer("smtp.gmail.com", 25, "jnolasco@neubox.net", "juanjosenolasco1234")

	if err := d.DialAndSend(m); err != nil {
		log.Println("(SendMail): Error sending mail - ", err)
	}
}

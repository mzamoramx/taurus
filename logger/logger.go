package logger

import (
	"log"

	"bitbucket.org/neuboxspacelab/taurus/configuration"
	"gopkg.in/gomail.v2"
)

var c = configuration.GetConfiguration()

// Email is used when we log some error to the admin
type Email struct {
	Message string
	Action  string
}

// ToEmail sends an email notification to the admin when an error happens
func ToEmail(message, action string) {
	e := &Email{
		Message: message,
		Action:  action,
	}
	if err := e.dialAndSend(e.message(c.EmailLog)); err != nil {
		log.Printf("Error sending email log notification %v", err.Error())
	}
}

func (e *Email) message(to string) *gomail.Message {
	m := gomail.NewMessage()
	m.SetAddressHeader("From", c.EmailLog, "Taurus")
	m.SetAddressHeader("To", to, to)
	m.SetHeader("Subject", "System Notifier")
	/*m.AddAlternativeWriter("text/html", func(w io.Writer) error {
		return m
	})*/
	return m
}

func (e *Email) dialAndSend(m *gomail.Message) error {
	d := gomail.NewDialer(
		c.EmailCredentials.Host,
		c.EmailCredentials.Port,
		c.EmailCredentials.UserName,
		c.EmailCredentials.Password,
	)

	if err := d.DialAndSend(m); err != nil {
		return err
	}
	return nil
}

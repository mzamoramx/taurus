package utils

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"

	"crypto/sha1"
	"encoding/base64"
	"strings"

	"runtime"

	"github.com/disintegration/imaging"
	"github.com/nicksnyder/go-i18n/i18n"
)

// ToJSON recieves a map and marshal it into json format, then send it to the user.
func ToJSON(res http.ResponseWriter, response interface{}, code int) {
	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(code)
	j, _ := json.Marshal(response)
	fmt.Fprintf(res, "%s", j)
}

// BackendTranslate Translate Any string.
func BackendTranslate(phrase string, args ...interface{}) (word string) {
	i18n.MustLoadTranslationFile("views/lang/es-mx.json")
	T, _ := i18n.Tfunc("es-MX")
	word = T(phrase, args...)
	return
}

// RenameImg rename img trough SHA1
func RenameImg(src string) (name string) {
	nsp := strings.Split(src, ".")

	compress := sha1.New()
	_, err := compress.Write([]byte("IMG" + string(nsp[0])))
	if err != nil {
		fmt.Println("(createNbxToken): ", err)
		return
	}

	name = base64.URLEncoding.EncodeToString(compress.Sum(nil)) + "." + nsp[1]

	return
}

// GenerateThumbnail generates a thumnail based on an image
func GenerateThumbnail(width, height int, imgName, path string, header *multipart.FileHeader) (thumbnail string, err error) {
	runtime.GOMAXPROCS(runtime.NumCPU())
	img, err := imaging.Open(imgName)
	if err != nil {
		fmt.Println(err, imgName)
		return
	}

	destimg := imaging.Resize(img, width, height, imaging.Box)
	if err = MkDirIfNotExists(path); err != nil {
		return
	}
	thumbnail = path + RenameImg(header.Filename)
	err = imaging.Save(destimg, thumbnail)
	if err != nil {
		return
	}
	return
}

// MkDirIfNotExists creates folder and/or folders recursively
func MkDirIfNotExists(path string) (err error) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		err = os.MkdirAll(path, os.ModePerm)
		return err
	}
	return
}

// CopyFile copies the contents of the file named src to the file named
// by dst. The file will be created if it does not already exist. If the
// destination file exists, all it's contents will be replaced by the contents
// of the source file. The file mode will be copied from the source and
// the copied data is synced/flushed to stable storage.
func CopyFile(src, dst string) (err error) {
	in, err := os.Open(src)
	if err != nil {
		return
	}
	defer in.Close()

	out, err := os.Create(dst)
	if err != nil {
		return
	}
	defer func() {
		if e := out.Close(); e != nil {
			err = e
		}
	}()

	_, err = io.Copy(out, in)
	if err != nil {
		return
	}

	err = out.Sync()
	if err != nil {
		return
	}

	si, err := os.Stat(src)
	if err != nil {
		return
	}
	err = os.Chmod(dst, si.Mode())
	if err != nil {
		return
	}

	return
}

// CopyDir recursively copies a directory tree, attempting to preserve permissions.
// Source directory must exist, destination directory must *not* exist.
// Symlinks are ignored and skipped.
func CopyDir(src string, dst string) (err error) {
	src = filepath.Clean(src)
	dst = filepath.Clean(dst)

	si, err := os.Stat(src)
	if err != nil {
		return err
	}
	if !si.IsDir() {
		return fmt.Errorf("source is not a directory")
	}

	_, err = os.Stat(dst)
	if err != nil && !os.IsNotExist(err) {
		return
	}
	if err == nil {
		return fmt.Errorf("destination already exists")
	}

	err = os.MkdirAll(dst, si.Mode())
	if err != nil {
		return
	}

	entries, err := ioutil.ReadDir(src)
	if err != nil {
		return
	}

	for _, entry := range entries {
		srcPath := filepath.Join(src, entry.Name())
		dstPath := filepath.Join(dst, entry.Name())

		if entry.IsDir() {
			err = CopyDir(srcPath, dstPath)
			if err != nil {
				return
			}
		} else {
			// Skip symlinks.
			if entry.Mode()&os.ModeSymlink != 0 {
				continue
			}

			err = CopyFile(srcPath, dstPath)
			if err != nil {
				return
			}
		}
	}

	return
}

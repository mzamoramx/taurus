package main

import (
	"time"

	"github.com/urfave/negroni"
	/* native */
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"

	"bitbucket.org/neuboxspacelab/taurus/routes"
)

/* ---------------------------------------------
Main
Version: 1.0
@authors Miguel Zamora
*/

//constants
const (
	DOMAIN = "localhost/" // Domain URL to write in responses
)

var (
	PIDFile = "/var/run/taurus.pid" // In case you want to implement a Daemon for the WebServer
	isLinux = flag.Bool("isLinux", false, "The operating system where the application is been executed, by default is Windows, if you're on Linux, you have to set it to true")
)

// Write the PID file for Daemon
func savePID(pid int) {
	file, err := os.Create(PIDFile)
	if err != nil {
		fmt.Println("Unable to create pid file: ", err)
		os.Exit(1)
	}

	defer file.Close()

	_, err = file.WriteString(strconv.Itoa(pid))

	if err != nil {
		fmt.Println("Unable to create pid file : ", err)
		os.Exit(1)
	}
	file.Sync() // flush to disk
}

//Welcome to the heaven ***
func main() {

	flag.Parse()

	//windows OS install Helper
	if *isLinux {
		savePID(os.Getpid())
	}

	mux := routes.InitRoutes()

	n := negroni.New()
	n.UseHandler(mux)

	/*config SSL
	cfg := &tls.Config{
		MinVersion:               tls.VersionTLS12,
		CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
		PreferServerCipherSuites: true,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
			tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_RSA_WITH_AES_256_CBC_SHA,
		},
	}

	srv := &http.Server{
		Addr:         ":443",
		Handler:      mux,
		TLSConfig:    cfg,
		TLSNextProto: make(map[string]func(*http.Server, *tls.Conn, http.Handler), 0),
	}
	log.Fatal(srv.ListenAndServeTLS("/certifies/linxz.crt", "/certifies/linxz.key"))
	*/
	//output log ************************** cambiadlo
	// HTTP Server Config
	srv := &http.Server{
		Addr:           ":8080",
		Handler:        n, // start Index
		ReadTimeout:    5 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 8175, // Limit Header Bytes so you can't be attacked with Header Overflows
	}

	if *isLinux {
		f, err := os.OpenFile("/var/log/taurus.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		if err != nil {
			fmt.Printf("error opening file: %v", err)
		}
		defer f.Close()
		log.SetOutput(f)
	}

	//serving on port server
	fmt.Println("Server running at " + srv.Addr)

	if err := srv.ListenAndServe(); err != nil {
		log.Fatal(err)
	}

}

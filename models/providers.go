package models

import (

	//"strconv"
	"fmt"

	"bitbucket.org/neuboxspacelab/taurus/configuration"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type (
	//Providers struct
	Providers struct {
		ID        bson.ObjectId `bson:"_id"`
		Name      string        `bson:"name"`
		Apikey    string        `bson:"API-KEY"`
		Secretkey string        `bson:"SECRET-KEY"`
	}
)

// ValidateProvider true if provider exists
func ValidateProvider(k string, s string) (provider *Providers, response bool) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["DBMain"]).C("providers")

	query := bson.M{"API-KEY": k, "SECRET-KEY": s}
	err = c.Find(query).One(&provider)
	if err != nil {
		fmt.Println("Header API Provider:", err)
		response = false
		return
	}

	response = true
	return
}

package models

import (
	"fmt"
	"time"

	"bitbucket.org/neuboxspacelab/taurus/configuration"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type (
	event struct {
		UserID  bson.ObjectId `bson:"userId"`
		IP      string        `bson:"ip"`
		Action  string        `bson:"action"`
		Message string        `bson:"message"`
		Time    time.Time     `bson:"time"`
	}
)

// Log logs information about the server status
func Log(userid bson.ObjectId, action, message, ip string) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("logs")

	ev := &event{
		UserID:  userid,
		IP:      ip,
		Action:  action,
		Message: message,
		Time:    time.Now(),
	}

	err = c.Insert(ev)
	if err != nil {
		fmt.Println("Error inserting log: ", err)
		return
	}
	return
}

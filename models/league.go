package models

import (
	"fmt"

	v "github.com/go-ozzo/ozzo-validation"

	"bitbucket.org/neuboxspacelab/taurus/configuration"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type (
	// League contain League general data
	League struct {
		ID        bson.ObjectId `bson:"_id"`
		Storage   bson.ObjectId `bson:"storage"`
		Name      string        `bson:"name" json:"name"`
		Countries []*Country    `bson:"countries" json:"countries"`
		Available bool          `bson:"available" json:"available"`
	}

	// Country defines available by country
	Country struct {
		Code   string `bson:"code" json:"code"`
		Enable bool   `bson:"enable" json:"enable"`
	}
)

// GetLeaguesCollection recives Json API
type GetLeaguesCollection struct {
	Filter string `json:"filter"`
}

// Validate GetLeaguesCollection
func (d GetLeaguesCollection) Validate() error {
	return v.ValidateStruct(
		&d,
		v.Field(&d.Filter, v.Required.Error("Required Field")),
	)
}

// JSONGetLeague recives Json API
type JSONGetLeague struct {
	ID string `json:"id"`
}

// Validate JSONGetLeague
func (d JSONGetLeague) Validate() error {
	return v.ValidateStruct(
		&d,
		v.Field(&d.ID, v.Required.Error("Required Field")),
	)
}

// JSONLeague recives CREATE Json API
type JSONLeague struct {
	Name      string     `json:"name"`
	Countries []*Country `json:"countries"`
	Available bool       `json:"available"`
}

// Validate League CREATE DATA
func (d JSONLeague) Validate() error {
	return v.ValidateStruct(
		&d,
		v.Field(&d.Name, v.Required.Error("Required")),
		v.Field(&d.Countries, v.Required.Error("Required")),
	)
}

// JSONLeagueUpdate recives UPDATE Json API
type JSONLeagueUpdate struct {
	ID        string     `json:"id"`
	Name      string     `json:"name"`
	Countries []*Country `json:"countries"`
	Available bool       `json:"available"`
}

// Validate League UPDATE DATA
func (d JSONLeagueUpdate) Validate() error {
	return v.ValidateStruct(
		&d,
		v.Field(&d.ID, v.Required.Error("Required")),
		v.Field(&d.Name, v.Required.Error("Required")),
		v.Field(&d.Countries, v.Required.Error("Required")),
	)
}

// Validate Country DATA
func (d Country) Validate() error {
	return v.ValidateStruct(
		&d,
		v.Field(&d.Code, v.Required.Error("Required")),
	)
}

// ListFullLeagues get all Leagues collection
func ListFullLeagues() (bsonRegs []bson.M, err error) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("leagues")

	err = c.Find(bson.M{}).All(&bsonRegs)
	if err != nil {
		fmt.Println("(GetLeaguesFullList): ", err)
		return
	}
	return
}

// Create creates the league in DB
func (l *League) Create() (err error) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("leagues")

	err = c.Insert(l)
	if err != nil {
		if mgo.IsDup(err) {
			//fmt.Println("Email already taken")
		} else {
			fmt.Println("(League:Create): Can't insert document: ", err)
		}
		return
	}
	return
}

// Update updates the league in DB stored
func (l *League) Update() (err error) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("leagues")
	query := bson.M{"_id": l.ID}
	change := bson.M{"$set": l}
	err = c.Update(query, change)
	if err != nil {
		fmt.Println("(leagueUpdate): Error trying to update the LEague - ", err)
		return
	}
	return

}

// LeagueExists returns the availability
func LeagueExists(name string) (exists bool, err error) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("leagues")

	query := bson.M{"name": name}
	count, err := c.Find(query).Count()
	if err != nil {
		return
	}
	// If count returns anything else than 0
	if count != 0 {
		exists = true
	} else {
		exists = false
	}
	return
}

// GetLeagueByID gets an league by id
func GetLeagueByID(uid bson.ObjectId) (l *League, err error) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("leagues")

	query := bson.M{"_id": uid}
	err = c.Find(query).One(&l)
	if err != nil {
		fmt.Println("(GetLeagueByID): ", err)
		l = nil
		return
	}
	return
}

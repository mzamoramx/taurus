package models

import (
	"encoding/base64"
	"fmt"
	"time"

	"crypto/sha512"

	"bitbucket.org/neuboxspacelab/taurus/configuration"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type (
	ResetToken struct {
		CreatedAt time.Time     `bson:"createdAt"`
		UserId    bson.ObjectId `bson:"userid"`
		Token     string        `bson:"token"`
	}
)

// GetResetPwdToken gets the Token for Reset Password functionality
func GetResetPwdToken(uid bson.ObjectId, email string) (token string, err error) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("resettkn")

	var tkn *ResetToken
	// We check if a token already exists for the User
	err = c.Find(bson.M{"userid": uid}).One(&tkn)
	if err != nil {
		// If not, create a token in DB
		tkn, err = createResetPwdToken(uid, email)
		if err != nil {
			fmt.Println("(GetResetPwdToken): ", err)
			return
		}
	}

	token = tkn.Token
	return
}

func createResetPwdToken(uid bson.ObjectId, email string) (tkn *ResetToken, err error) {
	token := sha512.New()
	_, err = token.Write([]byte(uid.String() + "SALT" + email)) // Seed for the sha512
	if err != nil {
		fmt.Println("(CreateResetPwdToken): ", err)
		return
	}

	tkn = &ResetToken{
		CreatedAt: time.Now(),
		UserId:    uid,
		Token:     base64.URLEncoding.EncodeToString(token.Sum(nil)),
	}

	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("resettkn")

	err = c.Insert(tkn)
	if err != nil {
		fmt.Println("(CreateResetPwdToken): Error inserting token to DB - ", err)
		return
	}
	return
}

// ValidResetPwdToken validates the token and email
func ValidResetPwdToken(uid bson.ObjectId, email, tkn string) (isValid bool) {
	isValid = false

	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("resettkn")

	// Check if token exists in DB
	i, err := c.Find(bson.M{"userid": uid}).Count()
	if err != nil || i == 0 {
		return
	}

	token := sha512.New()
	_, err = token.Write([]byte(uid.String() + "SALT" + email)) // Seed for the sha512
	if err != nil {
		fmt.Println("(ValidResetPwdToken): ", err)
		return
	}

	if tkn != base64.URLEncoding.EncodeToString(token.Sum(nil)) {
		fmt.Println("(ValidResetPwdToken): Token and Hash don't match")
		return
	}

	isValid = true
	return
}

// DeleteToken deletes the token from DB when used
func DeleteToken(uid bson.ObjectId) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("resettkn")

	// We check if a token already exists for the User
	err = c.Remove(bson.M{"userid": uid})
	if err != nil {
		fmt.Println("(DeleteToken): Error deleting token - ", err)
	}
	return
}

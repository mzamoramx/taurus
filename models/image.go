package models

import (

	/* go native */
	"bytes"
	"fmt"
	"io"
	"net/http"
	//"strconv"
	//"strings"
	//"mime/multipart"
	//"image/draw"
	//"image"
	//"image/jpeg"
	//"image/png"

	/* addons */
	"github.com/julienschmidt/httprouter"
	//"github.com/nfnt/resize"

	"bitbucket.org/neuboxspacelab/taurus/configuration"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

/* ---------------------------------------------
Files controller
Version: 1.0
@authors Felipe Leñero, Miguel Zamora
*/

func GetImage(res http.ResponseWriter, req *http.Request, p httprouter.Params) {
	if !bson.IsObjectIdHex(p.ByName("id")) {
		http.NotFound(res, req)
		return
	}

	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()
	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"])

	gridFile, err := c.GridFS("images").OpenId(bson.ObjectIdHex(p.ByName("id")))
	if err != nil {
		http.NotFound(res, req)
		return
	}

	buf := bytes.NewBuffer(nil)
	io.Copy(buf, gridFile)
	content := gridFile.ContentType()
	lastModified := gridFile.UploadDate()
	gridFile.Close()
	s := string(buf.Bytes())

	res.Header().Set("Vary", "Accept-Encoding")
	res.Header().Set("Cache-Control", "public, max-age=31536000")
	res.Header().Set("Last-Modified", lastModified.Format(http.TimeFormat))
	res.Header().Set("Content-type", content)
	fmt.Fprintf(res, "%s", s)
}

// GetUserImage gets the image of a specific user
func GetUserImage(res http.ResponseWriter, req *http.Request, p httprouter.Params) {
	if !bson.IsObjectIdHex(p.ByName("id")) {
		http.NotFound(res, req)
		return
	}

	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()
	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"])

	gridFile, err := c.GridFS("images").OpenId(bson.ObjectIdHex(p.ByName("id")))
	// If an avatar doesn't exists, load a default image.
	if err != nil {
		fileServer := http.FileServer(http.Dir("assets"))
		req.URL.Path = "img/avatar.svg"

		res.Header().Set("Vary", "Accept-Encoding")
		res.Header().Set("Cache-Control", "public, max-age=7200")
		fileServer.ServeHTTP(res, req)
		return
	}

	buf := bytes.NewBuffer(nil)
	io.Copy(buf, gridFile)
	content := gridFile.ContentType()
	lastModified := gridFile.UploadDate()
	gridFile.Close()
	s := string(buf.Bytes())

	res.Header().Set("Vary", "Accept-Encoding")
	res.Header().Set("Cache-Control", "public, max-age=31536000")
	res.Header().Set("Last-Modified", lastModified.Format(http.TimeFormat))
	res.Header().Set("Content-type", content)
	fmt.Fprintf(res, "%s", s)
}

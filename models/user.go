package models

import (
	"crypto/sha512"
	"encoding/base64"
	"fmt"
	"time"

	v "github.com/go-ozzo/ozzo-validation"

	"bitbucket.org/neuboxspacelab/taurus/configuration"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type (
	// User contain User general data
	User struct {
		ID        bson.ObjectId `bson:"_id"`
		Storage   bson.ObjectId `bson:"storage"`
		User      string        `bson:"user" json:"user"`
		Firstname string        `bson:"firstname" json:"firstname"`
		Lastname  string        `bson:"lastname" json:"lastname"`
		Email     string        `bson:"email" json:"email"`
		Phone     string        `bson:"phone" json:"phone"`
		Pwd       []byte        `bson:"pwd" json:"pwd"`
		Ine       string        `bson:"ine" json:"ine"`
		LastLogin time.Time     `bson:"lastlogin"`
		Verified  bool          `bson:"verified"`
		Address   *AddressData  `bson:"address" json:"address"`
		Balance   float64       `bson:"balance" json:"balance"`
		Country   string        `bson:"country" json:"country"`
	}

	// AddressData cointains address user data
	AddressData struct {
		Street    string `bson:"street" json:"street"`
		Number    string `bson:"number" json:"number"`
		Numberint string `bson:"numberint" json:"numberint"`
		Fracc     string `bson:"fracc" json:"fracc"`
		City      string `bson:"city" json:"city"`
		State     string `bson:"state" json:"state"`
		Country   string `bson:"country" json:"country"`
	}

	signupToken struct {
		UserId bson.ObjectId `bson:"userid"`
		Token  string        `bson:"token"`
	}
)

// GETUsersCollection recives Json API
type GETUsersCollection struct {
	Filter string `json:"filter"`
}

// Validate GETUsersCollection
func (d GETUsersCollection) Validate() error {
	return v.ValidateStruct(
		&d,
		v.Field(&d.Filter, v.Required.Error("Required Field")),
	)
}

// JSONGetUser recives Json API
type JSONGetUser struct {
	ID string `json:"id"`
}

// Validate JSONGetUser
func (d JSONGetUser) Validate() error {
	return v.ValidateStruct(
		&d,
		v.Field(&d.ID, v.Required.Error("Required Field")),
	)
}

// JSONUser recives CREATE Json API
type JSONUser struct {
	User      string       `json:"user"`
	Firstname string       `json:"firstname"`
	Lastname  string       `json:"lastname"`
	Email     string       `json:"email"`
	Phone     string       `json:"phone"`
	Pwd       string       `json:"pwd"`
	Ine       string       `json:"ine"`
	Country   string       `json:"country"`
	Address   *AddressData `json:"address"`
}

// Validate User CREATE DATA
func (d JSONUser) Validate() error {
	return v.ValidateStruct(
		&d,
		v.Field(&d.Firstname, v.Required.Error("Required")),
		v.Field(&d.Lastname, v.Required.Error("Required")),
		v.Field(&d.Pwd, v.Required.Error("Required")),
		v.Field(&d.Email, v.Required.Error("Required")),
		v.Field(&d.Phone, v.Required.Error("Required")),
		v.Field(&d.Ine, v.Required.Error("Required")),
		v.Field(&d.User, v.Required.Error("Required")),
		v.Field(&d.Country, v.Required.Error("Required")),
		v.Field(&d.Address, v.Required.Error("Required")),
	)
}

// JSONUserUpdate recives UPDATE Json API
type JSONUserUpdate struct {
	ID        string       `json:"id"`
	Firstname string       `json:"firstname"`
	Lastname  string       `json:"lastname"`
	Email     string       `json:"email"`
	Phone     string       `json:"phone"`
	Ine       string       `json:"ine"`
	Country   string       `json:"country"`
	Address   *AddressData `json:"address"`
}

// Validate User UPDATE DATA
func (d JSONUserUpdate) Validate() error {
	return v.ValidateStruct(
		&d,
		v.Field(&d.ID, v.Required.Error("Required")),
		v.Field(&d.Firstname, v.Required.Error("Required")),
		v.Field(&d.Lastname, v.Required.Error("Required")),
		v.Field(&d.Email, v.Required.Error("Required")),
		v.Field(&d.Phone, v.Required.Error("Required")),
		v.Field(&d.Country, v.Required.Error("Required")),
		v.Field(&d.Address, v.Required.Error("Required")),
	)
}

// JSONUpdateUsuario recives Json API
type JSONUpdateUsuario struct {
	ID   string `json:"id"`
	User string `json:"nickname"`
}

// Validate JSONUpdateUsuario
func (d JSONUpdateUsuario) Validate() error {
	return v.ValidateStruct(
		&d,
		v.Field(&d.ID, v.Required.Error("Required Field")),
		v.Field(&d.User, v.Required.Error("Required Field")),
	)
}

// JSONUpdatePwd recives Json API
type JSONUpdatePwd struct {
	ID  string `json:"id"`
	Pwd string `json:"pwd"`
}

// Validate JSONUpdatePwd
func (d JSONUpdatePwd) Validate() error {
	return v.ValidateStruct(
		&d,
		v.Field(&d.ID, v.Required.Error("Required Field")),
		v.Field(&d.Pwd, v.Required.Error("Required Field")),
	)
}

// JSONLogin recives Json API
type JSONLogin struct {
	Email string `json:"email"`
	Pwd   string `json:"pwd"`
}

// Validate JSONLogin
func (d JSONLogin) Validate() error {
	return v.ValidateStruct(
		&d,
		v.Field(&d.Email, v.Required.Error("Required Field")),
		v.Field(&d.Pwd, v.Required.Error("Required Field")),
	)
}

// Validate User DATA
func (d AddressData) Validate() error {
	return v.ValidateStruct(
		&d,
		v.Field(&d.Street, v.Required.Error("Required")),
		v.Field(&d.Number, v.Required.Error("Required")),
		v.Field(&d.Numberint, v.Required.Error("Required")),
		v.Field(&d.Fracc, v.Required.Error("Required")),
		v.Field(&d.City, v.Required.Error("Required")),
		v.Field(&d.State, v.Required.Error("Required")),
		v.Field(&d.Country, v.Required.Error("Required")),
	)
}

// ListFullUsers get all Users collection
func ListFullUsers() (bsonRegs []bson.M, err error) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("users")

	err = c.Find(bson.M{}).All(&bsonRegs)
	if err != nil {
		fmt.Println("(GetUserByEmail): ", err)
		return
	}
	return
}

// Create creates the user in DB stored in the User Interface
func (u *User) Create() (err error) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("users")

	err = c.Insert(u)
	if err != nil {
		if mgo.IsDup(err) {
			//fmt.Println("Email already taken")
		} else {
			fmt.Println("(User:Create): Can't insert document: ", err)
		}
		return
	}
	return
}

// Update updates the user in DB stored in the User Interface
func (u *User) Update() (err error) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("users")
	query := bson.M{"_id": u.ID}
	change := bson.M{"$set": u}
	err = c.Update(query, change)
	if err != nil {
		fmt.Println("(user.Update): Error trying to update the User - ", err)
		return
	}
	return

}

// SetPassword Takes a plaintext password and hashes it with bcrypt and sets the
// password field to the hash.
func (u *User) SetPassword(password string) {
	hpass, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		fmt.Println("(SetPassword): ", err)
		return
	}
	u.Pwd = hpass
}

// UpdatePassword updates the user's password
func (u *User) UpdatePassword(password string) (err error) {
	u.SetPassword(password)

	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()
	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("users")

	query := bson.M{"_id": u.ID}
	change := bson.M{"$set": bson.M{"pwd": u.Pwd}}
	err = c.Update(query, change)
	if err != nil {
		fmt.Println("(user.UpdatePassword): Error trying to update Password - ", err)
		return
	}

	return
}

// Login validates and returns an user object if they exist in the DB.
// Also updates the LastLogin field in the DB
func Login(email, password string) (u *User, err error) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("users")

	query := bson.M{"email": email}
	err = c.Find(query).One(&u)
	if err != nil {
		// TO-DO
		// Implement a system to detect repeated failed logins and
		// ban the IP for a certain time
		fmt.Println("error login")
		return
	}
	// Compare User Hashed Password and Password Hash given
	err = bcrypt.CompareHashAndPassword(u.Pwd, []byte(password))
	if err != nil {
		return
	}

	/*
		if !u.Verified {
			u = nil
			return
		}
	*/

	// Updates the LastLogin field
	change := bson.M{"$set": bson.M{"lastlogin": time.Now()}}
	err = c.Update(query, change)
	if err != nil {
		err = nil
		fmt.Println("Couldn't update lastlogin")
		return
	}

	return
}

// Login validates and returns an user object if they exist in the DB.
// Also updates the LastLogin field in the DB
func LoginHash(tkn string) (u *User, err error) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("users")

	query := bson.M{"nbxuser.nbx_tkn": tkn}
	err = c.Find(query).One(&u)

	if err != nil {
		// TO-DO
		// Implement a system to detect repeated failed logins and
		// ban the IP for a certain time
		fmt.Println("error login")
		return
	}

	// Updates the LastLogin field
	change := bson.M{"$set": bson.M{"lastlogin": time.Now()}}
	err = c.Update(query, change)
	if err != nil {
		err = nil
		fmt.Println("Couldn't update lastlogin")
		return
	}

	return
}

// UserExists returns the email availability
// Created in order to reduce CPU consumption and resources used in
// bcrypt.GenerateFromPassword() from SetPassword while trying to create
// the user, just to found out that the user email was already taken.
func UserExists(email string) (exists bool, err error) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("users")

	query := bson.M{"email": email}
	count, err := c.Find(query).Count()
	if err != nil {
		return
	}
	// If count returns anything else than 0 UserExists
	if count != 0 {
		exists = true
	} else {
		exists = false
	}
	return
}

// GetUserByEmail gets the user by email
func GetUserByEmail(email string) (u *User, err error) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("users")

	query := bson.M{"email": email}
	err = c.Find(query).One(&u)
	if err != nil {
		fmt.Println("(GetUserByEmail): ", err)
		u = nil
		return
	}
	return
}

// GetUserById gets an user by id
func GetUserById(uid bson.ObjectId) (u *User, err error) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("users")

	query := bson.M{"_id": uid}
	err = c.Find(query).One(&u)
	if err != nil {
		fmt.Println("(GetUserById): ", err)
		u = nil
		return
	}
	return
}

// GetUserSess gets the user's session
func GetUserSess(uid bson.ObjectId) (u *User, err error) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("users")

	query := bson.M{"_id": uid}
	err = c.Find(query).One(&u)
	if err != nil {
		fmt.Println("(GetUserSess): ", err)
		u = nil
		return
	}
	return
}

// GetSignupToken gets the user's token when signed up
func GetSignupToken(uid bson.ObjectId, email string) (token string) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("signuptkn")

	var tkn *signupToken
	// We check if a token already exists for the User
	err = c.Find(bson.M{"userid": uid}).One(&tkn)
	if err != nil {
		// If not, create a token in DB
		tkn, err = createSignupToken(uid, email)
		if err != nil {
			fmt.Println("(GetSignupToken): ", err)
			return
		}
	}

	token = tkn.Token
	return
}

// Creates a SignupToken to validate the user account via Email
func createSignupToken(uid bson.ObjectId, email string) (tkn *signupToken, err error) {
	token := sha512.New()
	_, err = token.Write([]byte(uid.String() + "SALT" + email))
	if err != nil {
		fmt.Println("(createSignupToken): ", err)
		return
	}

	tkn = &signupToken{
		UserId: uid,
		Token:  base64.URLEncoding.EncodeToString(token.Sum(nil)),
	}

	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("signuptkn")

	err = c.Insert(tkn)
	if err != nil {
		fmt.Println("(createSignupToken): Error inserting token to DB - ", err)
		return
	}
	return
}

// ValidSignupToken validates the user's token when signed up
func ValidSignupToken(uid bson.ObjectId, email, tkn string) (isValid bool) {
	isValid = false

	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("signuptkn")

	// Check if token exists in DB
	i, err := c.Find(bson.M{"userid": uid}).Count()
	if err != nil || i == 0 {
		return
	}

	token := sha512.New()
	_, err = token.Write([]byte(uid.String() + "SALT" + email))
	if err != nil {
		fmt.Println("(ValidSignupToken): ", err)
		return
	}

	if tkn != base64.URLEncoding.EncodeToString(token.Sum(nil)) {
		fmt.Println("(ValidSignupToken): Token and Hash don't match")
		return
	}

	isValid = true
	return
}

// VerifyAccount verifies the user's account
func VerifyAccount(uid bson.ObjectId, ip string) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	// Update the verified status of user
	c := dbsess.DB(d.Databases["db_main"]).C("users")
	query := bson.M{"_id": uid}
	change := bson.M{"$set": bson.M{"verified": true}}
	err = c.Update(query, change)
	if err != nil {
		fmt.Println("(VerifyAccount): Error trying to update verification status - ", err)
		return
	}

	Log(uid, "verify account", "success", ip)

	// Then delete the signup token
	c = dbsess.DB(d.Databases["db_main"]).C("signuptkn")
	err = c.Remove(bson.M{"userid": uid})
	if err != nil {
		fmt.Println("(VerifyAccount): Error deleting token - ", err)
	}
	return
}

func UpdateName(uid bson.ObjectId, nameUser string) error {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return err
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})

	c := dbsess.DB(d.Databases["db_main"]).C("users")
	query := bson.M{"_id": uid}
	change := bson.M{"$set": bson.M{"name": nameUser}}
	err = c.Update(query, change)
	if err != nil {
		fmt.Println("(updateName): Error trying to update name - ", err)
		return err
	}
	return err
}

func VerifyCurrentPassword(uid bson.ObjectId, pwd string) (u *User, err error) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("users")

	query := bson.M{"_id": uid}
	err = c.Find(query).One(&u)
	if err != nil {
		fmt.Println("user not found")
		return
	}

	err = bcrypt.CompareHashAndPassword(u.Pwd, []byte(pwd))
	if err != nil {
		fmt.Println("Passwords are different")
		return
	}
	return
}

func SaveContactEmail(uid bson.ObjectId, contactEmail string) error {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return err
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})

	c := dbsess.DB(d.Databases["db_main"]).C("users")
	query := bson.M{"_id": uid}
	change := bson.M{"$set": bson.M{"contactemail": contactEmail}}
	err = c.Update(query, change)
	if err != nil {
		fmt.Println("(SaveContactEmail): Error trying to update contactemail - ", err)
		return err
	}
	return err
}

/*
func navDataUser(uid bson.ObjectId, email string) (token string) {

	var tkn *signupToken
	// We check if a token already exists for the User
	err = c.Find(bson.M{"userid": uid}).One(&tkn)
	if err != nil {
		// If not, create a token in DB
		tkn, err = createSignupToken(uid, email)
		if err != nil {
			fmt.Println("(GetSignupToken): ", err)
			return
		}
	}

	token = tkn.Token
	return
}
*/

//DeleteUser delete users at cascade algoritm creation falling
func DeleteUser(uid bson.ObjectId) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	s := dbsess.DB(d.Databases["DBMain"]).C("users")

	err = s.Remove(bson.M{"_id": uid})
	if err != nil {
		fmt.Println("(DeleteUser): Error deleting user - ", err)
	}
	return
}

package models

import "bitbucket.org/neuboxspacelab/taurus/configuration"

var d configuration.Configuration

func init() {
	d = configuration.GetConfiguration()
}

package models

import (
	"fmt"
	"time"

	v "github.com/go-ozzo/ozzo-validation"

	"bitbucket.org/neuboxspacelab/taurus/configuration"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type (
	// Journey contain journey general data
	Journey struct {
		ID       bson.ObjectId `bson:"_id"`
		Event    int           `bson:"event" json:"event"`
		LeagueID bson.ObjectId `bson:"leagueid"`
		Start    time.Time     `bson:"start" json:"start"`
		End      time.Time     `bson:"end" json:"end"`
		Cron     time.Time     `bson:"cron" json:"cron"`
		Status   string        `bson:"status" json:"status"`
	}
)

// GetJourneysCollection recives Json API
type GetJourneysCollection struct {
	Filter string `json:"filter"`
}

// Validate GetJourneysCollection
func (d GetJourneysCollection) Validate() error {
	return v.ValidateStruct(
		&d,
		v.Field(&d.Filter, v.Required.Error("Required Field")),
	)
}

// JSONGetJourney recives Json API
type JSONGetJourney struct {
	ID string `json:"id"`
}

// Validate JSONGetJourney
func (d JSONGetJourney) Validate() error {
	return v.ValidateStruct(
		&d,
		v.Field(&d.ID, v.Required.Error("Required Field")),
	)
}

// JSONJourney recives CREATE Json API
type JSONJourney struct {
	Event    int       `json:"event"`
	LeagueID string    `json:"leagueid"`
	Start    time.Time `json:"start"`
	End      time.Time `json:"end"`
	Cron     time.Time `json:"cron"`
	Status   string    `json:"status"`
}

// Validate Journey CREATE DATA
func (d JSONJourney) Validate() error {
	return v.ValidateStruct(
		&d,
		v.Field(&d.Event, v.Required.Error("Required")),
		v.Field(&d.LeagueID, v.Required.Error("Required")),
		v.Field(&d.Start, v.Required.Error("Required")),
		v.Field(&d.End, v.Required.Error("Required")),
		v.Field(&d.Cron, v.Required.Error("Required")),
		v.Field(&d.Status, v.Required.Error("Required")),
	)
}

// JSONJourneyUpdate recives UPDATE Json API
type JSONJourneyUpdate struct {
	ID       string    `json:"id"`
	Event    int       `json:"event"`
	LeagueID string    `json:"leagueid"`
	Start    time.Time `json:"start"`
	End      time.Time `json:"end"`
	Cron     time.Time `json:"cron"`
	Status   string    `json:"status"`
}

// Validate Journey UPDATE DATA
func (d JSONJourneyUpdate) Validate() error {
	return v.ValidateStruct(
		&d,
		v.Field(&d.ID, v.Required.Error("Required")),
		v.Field(&d.Event, v.Required.Error("Required")),
		v.Field(&d.LeagueID, v.Required.Error("Required")),
		v.Field(&d.Start, v.Required.Error("Required")),
		v.Field(&d.End, v.Required.Error("Required")),
		v.Field(&d.Cron, v.Required.Error("Required")),
		v.Field(&d.Status, v.Required.Error("Required")),
	)
}

// ListFullJourneys get all journeys collection
func ListFullJourneys() (bsonRegs []bson.M, err error) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("journeys")

	err = c.Find(bson.M{}).All(&bsonRegs)
	if err != nil {
		fmt.Println("(GetJourneysFullList): ", err)
		return
	}
	return
}

// Create creates the journey in DB
func (j *Journey) Create() (err error) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("journeys")

	err = c.Insert(j)
	if err != nil {
		if mgo.IsDup(err) {
			//fmt.Println("Email already taken")
		} else {
			fmt.Println("(JourneysCreate): Can't insert document: ", err)
		}
		return
	}
	return
}

// Update updates the journey in DB stored
func (j *Journey) Update() (err error) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("journeys")
	query := bson.M{"_id": j.ID}
	change := bson.M{"$set": j}
	err = c.Update(query, change)
	if err != nil {
		fmt.Println("(journeyUpdate): Error trying to update the Journey - ", err)
		return
	}
	return

}

// JourneyExists returns the availability
func JourneyExists(event int) (exists bool, err error) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("journeys")

	query := bson.M{"event": event}
	count, err := c.Find(query).Count()
	if err != nil {
		return
	}
	// If count returns anything else than 0
	if count != 0 {
		exists = true
	} else {
		exists = false
	}
	return
}

// GetJourneyByID gets an journey by id
func GetJourneyByID(uid bson.ObjectId) (j *Journey, err error) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	dbsess.SetSafe(&mgo.Safe{})
	c := dbsess.DB(d.Databases["db_main"]).C("journeys")

	query := bson.M{"_id": uid}
	err = c.Find(query).One(&j)
	if err != nil {
		fmt.Println("(GetJourneyByID): ", err)
		j = nil
		return
	}
	return
}

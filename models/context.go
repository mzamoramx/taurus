package models

import (
	"errors"
	"fmt"
	"net/http"

	"bitbucket.org/neuboxspacelab/taurus/configuration"
	"github.com/kidstuff/mongostore"
	"gopkg.in/mgo.v2/bson"
)

type (
	// Context holds information about site's global state
	Context struct {
		ID            string
		Email         string
		LastLogin     string
		ResetPwdToken string      // To keep a temporary session to reset password from Forgot Password Form
		User          *User       // Session User
		Data          interface{} // Data for controllers
		Appends       interface{} // Data for controllers
		CurrentUid    string      // store uid current App, site or Store
		Storage       string      // storage folder user
		Info          *Info       // storage folder user
	}

	// LogoSite holds information about site's logo
	LogoSite struct {
		Uid    bson.ObjectId
		Domain string
		Logo   string
	}

	//Info for builder
	Info struct {
		Uid         string
		Nbx         bool
		Layout      interface{}
		Edit        interface{}
		Render      interface{}
		Name        string
		Nomodule    int
		ServerURI   string
		SiteID      string
		Step        int
		HaveHosting bool
		PublishURL  string
		DataUser    *User
	}
)

// NewSession creates a session for the user and return the session ID
func NewSession(res http.ResponseWriter, req *http.Request, user *User) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	store := mongostore.NewMongoStore(
		dbsess.DB(d.Databases["db_main"]).C("sessions"),
		3600*24, true, []byte("SECRET-SALT"))

	// Get a session.
	session, err := store.Get(req, "session")
	if err != nil {
		fmt.Println(err)
		return
	}

	// Add values to Session.
	session.Values["id"] = user.ID.Hex()
	session.Values["lastlogin"] = user.LastLogin.Format("_2 Jan, 2006 - 15:04")
	session.Options.MaxAge = 3600 * 24

	if err = session.Save(req, res); err != nil {
		fmt.Println("(NewSession): Error saving session: ", err)
	}
}

// DestroySession unset the user's session
func DestroySession(res http.ResponseWriter, req *http.Request) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	store := mongostore.NewMongoStore(
		dbsess.DB(d.Databases["db_main"]).C("sessions"),
		3600*24, true, []byte("SECRET-SALT"))

	// Get a session.
	session, err := store.Get(req, "session")
	if err != nil {
		fmt.Println(err)
		return
	}

	session.Options.MaxAge = -1 // This destroys the session
	// Save
	if err = session.Save(req, res); err != nil {
		fmt.Println("Error Destroying Session: ", err)
	}
}

// GetSession gets the user's session
func GetSession(req *http.Request) (ctx *Context, err error) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	store := mongostore.NewMongoStore(
		dbsess.DB(d.Databases["db_main"]).C("sessions"),
		3600*24, true, []byte("SECRET-SALT"))

	// Get a session.
	session, err := store.Get(req, "session")
	if err != nil || session.IsNew == true {
		err = errors.New("Session not found")
		return
	}
	ctx = &Context{
		ID:        session.Values["id"].(string),
		LastLogin: session.Values["lastlogin"].(string),
	}

	//add user & account
	user, err := GetUserSess(bson.ObjectIdHex(ctx.ID))
	if err == nil {
		//response
	}

	ctx.User = user

	return
}

// UpdateSession in case you need to update/refresh session stored values
func UpdateSession(res http.ResponseWriter, req *http.Request, user *User) {
	dbsess, err := configuration.GetConnection()
	if err != nil {
		fmt.Println("Error Connecting to the DB")
		return
	}
	defer dbsess.Close()

	store := mongostore.NewMongoStore(
		dbsess.DB(d.Databases["db_main"]).C("sessions"),
		3600*24, true, []byte("SECRET-SALT"))

	// Get the session.
	session, err := store.Get(req, "session")
	if err != nil || session.IsNew == true {
		err = errors.New("Session not found")
		return
	}
	// Reload values to Session.
	session.Values["id"] = user.ID.Hex()
	session.Values["lastlogin"] = user.LastLogin.Format("_2 Jan, 2006 - 15:04")
	session.Options.MaxAge = 3600 * 24

	// Save.
	if err = session.Save(req, res); err != nil {
		fmt.Println("(UpdateSession): Error updating session: ", err)
	}

	return
}

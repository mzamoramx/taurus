package routes

import (
	api "bitbucket.org/neuboxspacelab/taurus/api"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

// SetAPILeaguesRoutes sets up api's leagues routes
func SetAPILeaguesRoutes(router *mux.Router) *mux.Router {
	prefix := "/leagues"
	apiRouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	apiRouter.HandleFunc("/", api.LeagueGetAll).Methods("POST")
	apiRouter.HandleFunc("/get", api.LeagueGet).Methods("POST")
	apiRouter.HandleFunc("/create", api.LeagueCreate).Methods("POST")
	apiRouter.HandleFunc("/update", api.LeagueUpdate).Methods("POST")
	router.PathPrefix(prefix).Handler(negroni.New(
		negroni.Wrap(apiRouter),
	))

	return router
}

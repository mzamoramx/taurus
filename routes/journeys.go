package routes

import (
	api "bitbucket.org/neuboxspacelab/taurus/api"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

// SetAPIJourneysRoutes sets up api's journeys routes
func SetAPIJourneysRoutes(router *mux.Router) *mux.Router {
	prefix := "/journeys"
	apiRouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	apiRouter.HandleFunc("/", api.JourneyGetAll).Methods("POST")
	apiRouter.HandleFunc("/get", api.JourneyGet).Methods("POST")
	apiRouter.HandleFunc("/create", api.JourneyCreate).Methods("POST")
	apiRouter.HandleFunc("/update", api.JourneyUpdate).Methods("POST")
	router.PathPrefix(prefix).Handler(negroni.New(
		negroni.Wrap(apiRouter),
	))

	return router
}

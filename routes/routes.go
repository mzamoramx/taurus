package routes

import (
	"github.com/gorilla/mux"
)

// InitRoutes initializes the project's routes and returns a new router
func InitRoutes() *mux.Router {
	router := mux.NewRouter().StrictSlash(false)
	SetAPILeaguesRoutes(router)
	SetAPIJourneysRoutes(router)
	SetAPIUserRoutes(router)
	return router
}

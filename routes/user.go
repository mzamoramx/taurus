package routes

import (
	api "bitbucket.org/neuboxspacelab/taurus/api"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

// SetAPIUserRoutes sets up api's routes
func SetAPIUserRoutes(router *mux.Router) *mux.Router {
	prefix := "/users"
	apiRouter := mux.NewRouter().PathPrefix(prefix).Subrouter().StrictSlash(true)
	apiRouter.HandleFunc("/", api.GetAll).Methods("POST")
	apiRouter.HandleFunc("/get", api.Get).Methods("POST")
	apiRouter.HandleFunc("/create", api.Create).Methods("POST")
	apiRouter.HandleFunc("/update", api.Update).Methods("POST")
	apiRouter.HandleFunc("/setnickname", api.SetUserNickName).Methods("POST")
	apiRouter.HandleFunc("/setpwd", api.SetUserPwd).Methods("POST")
	apiRouter.HandleFunc("/login", api.Login).Methods("POST")
	router.PathPrefix(prefix).Handler(negroni.New(
		negroni.Wrap(apiRouter),
	))

	return router
}

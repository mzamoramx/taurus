package upload

import (
	"errors"
	"io"
	"mime/multipart"
	"net/http"

	"os"

	"bitbucket.org/neuboxspacelab/taurus/utils"
)

// File function uploads a single file in the path specified.
func File(res http.ResponseWriter, header *multipart.FileHeader, path string) (name string, err error) {
	file, err := header.Open()
	if err != nil {
		err = errors.New(utils.BackendTranslate("read-file"))
	}
	defer file.Close()

	name = utils.RenameImg(header.Filename)
	dest, err := os.Create(path + name)
	if err != nil {
		err = errors.New(utils.BackendTranslate("storage-path"))
	}
	defer dest.Close()

	_, err = io.Copy(dest, file)
	if err != nil {
		err = errors.New(utils.BackendTranslate("storage-path"))
	}

	return
}

// IsValidFormat checks if the file sent by the user has a valid mime type
func IsValidFormat(mimeType string) bool {
	switch mimeType {
	case "image/jpeg", "image/jpg", "image/png", "image/svg+xml":
		return true
	}
	return false
}

// IsValidSize checks if the file sent by the user has a valid size
func IsValidSize(file multipart.File) bool {
	const maxSizeAllowed = 2097152 // ~2mb
	size, err := file.Seek(0, io.SeekEnd)
	if err != nil {
		return false
	}
	if size > maxSizeAllowed {
		return false
	}
	return true
}

package api

import (

	/* native */

	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"os"
	"regexp"

	/* addons */

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	/* home */

	"bitbucket.org/neuboxspacelab/taurus/logger"
	"bitbucket.org/neuboxspacelab/taurus/models"
	"bitbucket.org/neuboxspacelab/taurus/utils"
)

// GetAll check if Neubox user exists
func GetAll(res http.ResponseWriter, req *http.Request) {

	//get Headers
	apiKey := req.Header.Get("API-KEY")
	secretKey := req.Header.Get("SECRET-KEY")

	//validate headers
	_, access := models.ValidateProvider(apiKey, secretKey)

	//is a valid provider
	if access {

		//json data
		d := models.GETUsersCollection{}
		err := json.NewDecoder(req.Body).Decode(&d)
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": "Bad request, check Json Struct"}
			JSONresponse(res, result)
			return
		}

		//Validate JSON fields
		err = d.Validate()
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": err.Error()}
			JSONresponse(res, result)
			return
		}

		//get U model, all users!
		c, err := models.ListFullUsers()
		if err != nil {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": err.Error()}
			JSONresponse(res, result)
			return
		}

		//parse into JSON
		users, err := json.Marshal(c)
		if err != nil {
			res.WriteHeader(http.StatusAccepted)
			result := map[string]string{"status": "success", "message": "Not users yet"}
			JSONresponse(res, result)
			return
		}

		//success!!
		res.WriteHeader(http.StatusAccepted)
		result := map[string]string{"status": "succes", "data": string(users), "message": "Data served ::YouROCK::"}
		JSONresponse(res, result)
		return

	}

	//forbidden
	res.WriteHeader(http.StatusForbidden)
	result := map[string]string{"status": "error", "message": "You don't have access"}
	JSONresponse(res, result)
	return
}

//Create a New User
func Create(res http.ResponseWriter, req *http.Request) {

	//get Headers
	apiKey := req.Header.Get("API-KEY")
	secretKey := req.Header.Get("SECRET-KEY")

	//validate headers
	_, access := models.ValidateProvider(apiKey, secretKey)

	//is a valid provider
	if access {

		//json data
		d := models.JSONUser{}
		err := json.NewDecoder(req.Body).Decode(&d)
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": "Bad request, check Json Struct"}
			JSONresponse(res, result)
			return
		}

		//Validate JSON fields
		err = d.Validate()
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": err.Error()}
			JSONresponse(res, result)
			return
		}

		//Validate Address JSON fields
		err = d.Address.Validate()
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": err.Error()}
			JSONresponse(res, result)
			return
		}

		//mail validation
		if len(d.Email) > 254 {
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": "email don't must to contain more than 254 characters"}
			JSONresponse(res, result)
			return
		}

		if m, _ := regexp.MatchString(`^.+@.+\..+`, d.Email); !m {
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": "please send a valid email."}
			JSONresponse(res, result)
			return
		}

		//user validation
		if exists, err := models.UserExists(d.Email); err != nil {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": "Error to review email, try again."}
			JSONresponse(res, result)
			return
		} else if exists {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": "mail already taken, we can't create the user :("}
			JSONresponse(res, result)
			return
		}

		//Creating User *****
		user := &models.User{
			ID:        bson.NewObjectId(),
			Storage:   bson.NewObjectId(),
			User:      d.User,
			Firstname: d.Firstname,
			Lastname:  d.Lastname,
			Email:     d.Email,
			Phone:     d.Phone,
			Ine:       d.Ine,
			Verified:  false,
			Address: &models.AddressData{
				Street:    d.Address.Street,
				Number:    d.Address.Number,
				Numberint: d.Address.Numberint,
				Fracc:     d.Address.Fracc,
				City:      d.Address.City,
				State:     d.Address.State,
				Country:   d.Address.Country,
			},
			Country: d.Country,
			Balance: 0,
		}

		//pass
		user.SetPassword(d.Pwd)

		err = user.Create()
		if err != nil {
			if mgo.IsDup(err) {
				res.WriteHeader(http.StatusConflict)
				result := map[string]string{"status": "error", "message": "Email already taken in DB"}
				JSONresponse(res, result)
				return
			}
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": "Error trying to create the user, try again."}
			JSONresponse(res, result)
			logger.ToEmail(err.Error()+"File users.go line 197", "Couldn't create User via API")
			return
		}

		//obtaing IP
		ip, _, err := net.SplitHostPort(req.RemoteAddr)
		if err != nil {
			ip = "0.0.0.0"
			fmt.Println("userip: is not IP:port", req.RemoteAddr)
		}

		//saving log
		models.Log(user.ID, "created", "success", ip)

		//create user folder
		path := "storage/" + user.Storage.Hex()
		if err := utils.MkDirIfNotExists(path); err != nil {
			destPath, errPath := os.Create(path)

			if errPath != nil {
				fmt.Println(errPath)
			} else {
				fmt.Println("Dir Storage created")
			}
			destPath.Close()
		}

		//SUCCES user created
		res.WriteHeader(http.StatusCreated)
		//base 64 TOKEN
		result := map[string]string{"status": "success", "message": "User created succesfully", "id": user.ID.Hex()}
		JSONresponse(res, result)
		return

	}

	//forbidden
	res.WriteHeader(http.StatusForbidden)
	result := map[string]string{"status": "error", "message": "You don't have acces"}
	JSONresponse(res, result)
	return
}

//Update data User
func Update(res http.ResponseWriter, req *http.Request) {

	//get Headers
	apiKey := req.Header.Get("API-KEY")
	secretKey := req.Header.Get("SECRET-KEY")

	//validate headers
	_, access := models.ValidateProvider(apiKey, secretKey)

	//is a valid provider
	if access {

		//json data
		d := models.JSONUserUpdate{}
		err := json.NewDecoder(req.Body).Decode(&d)
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": "Bad request, check Json Struct"}
			JSONresponse(res, result)
			return
		}

		//Validate JSON fields
		err = d.Validate()
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": err.Error()}
			JSONresponse(res, result)
			return
		}

		//Validate Address JSON fields
		err = d.Address.Validate()
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": err.Error()}
			JSONresponse(res, result)
			return
		}

		//mail validation
		if len(d.Email) > 254 {
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": "email don't must to contain more than 254 characters"}
			JSONresponse(res, result)
			return
		}

		if m, _ := regexp.MatchString(`^.+@.+\..+`, d.Email); !m {
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": "please send a valid email."}
			JSONresponse(res, result)
			return
		}

		//user validation TODO VALIDATE STRING BEFORE APPLY BSON ***//
		user, error := models.GetUserById(bson.ObjectIdHex(d.ID))

		if error != nil {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": "Error to get user, invalid ID."}
			JSONresponse(res, result)
			return
		}

		//Updating data User *****
		user.Firstname = d.Firstname
		user.Lastname = d.Lastname
		user.Email = d.Email
		user.Phone = d.Phone
		user.Country = d.Country
		user.Address = d.Address

		err = user.Update()
		if err != nil {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": "Error trying to update the user, try again."}
			JSONresponse(res, result)
			logger.ToEmail(err.Error()+"File users.go line 321", "Couldn't update User via API")
			return
		}

		//obtaing IP
		ip, _, err := net.SplitHostPort(req.RemoteAddr)
		if err != nil {
			ip = "0.0.0.0"
			fmt.Println("userip: is not IP:port", req.RemoteAddr)
		}

		//saving log
		models.Log(user.ID, "update", "success", ip)

		//SUCCES user updated
		res.WriteHeader(http.StatusAccepted)
		//base 64 TOKEN
		result := map[string]string{"status": "success", "message": "User updated succesfully", "id": user.ID.Hex()}
		JSONresponse(res, result)
		return

	}

	//forbidden
	res.WriteHeader(http.StatusForbidden)
	result := map[string]string{"status": "error", "message": "You don't have acces"}
	JSONresponse(res, result)
	return
}

//Get data User
func Get(res http.ResponseWriter, req *http.Request) {

	//get Headers
	apiKey := req.Header.Get("API-KEY")
	secretKey := req.Header.Get("SECRET-KEY")

	//validate headers
	_, access := models.ValidateProvider(apiKey, secretKey)

	//is a valid provider
	if access {

		//json data
		d := models.JSONGetUser{}
		err := json.NewDecoder(req.Body).Decode(&d)
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": "Bad request, check Json Struct"}
			JSONresponse(res, result)
			return
		}

		//Validate JSON fields
		err = d.Validate()
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": err.Error()}
			JSONresponse(res, result)
			return
		}

		//user validation TODO VALIDATE STRING BEFORE APPLY BSON ***//
		user, error := models.GetUserById(bson.ObjectIdHex(d.ID))

		if error != nil {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": "Error to get user, invalid ID."}
			JSONresponse(res, result)
			return
		}

		//parse into JSON
		u, err := json.Marshal(user)
		if err != nil {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "success", "message": "Error to parsed user"}
			JSONresponse(res, result)
			return
		}

		//success!!
		res.WriteHeader(http.StatusAccepted)
		result := map[string]string{"status": "succes", "data": string(u), "message": "User served ::YouROCK::"}
		JSONresponse(res, result)
		return

	}

	//forbidden
	res.WriteHeader(http.StatusForbidden)
	result := map[string]string{"status": "error", "message": "You don't have acces"}
	JSONresponse(res, result)
	return
}

//SetUserNickName reset data User
func SetUserNickName(res http.ResponseWriter, req *http.Request) {

	//get Headers
	apiKey := req.Header.Get("API-KEY")
	secretKey := req.Header.Get("SECRET-KEY")

	//validate headers
	_, access := models.ValidateProvider(apiKey, secretKey)

	//is a valid provider
	if access {

		//json data
		d := models.JSONUpdateUsuario{}
		err := json.NewDecoder(req.Body).Decode(&d)
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": "Bad request, check Json Struct"}
			JSONresponse(res, result)
			return
		}

		//Validate JSON fields
		err = d.Validate()
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": err.Error()}
			JSONresponse(res, result)
			return
		}

		//user validation TODO VALIDATE STRING BEFORE APPLY BSON ***//
		user, error := models.GetUserById(bson.ObjectIdHex(d.ID))
		if error != nil {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": "Error to get user, invalid ID."}
			JSONresponse(res, result)
			return
		}

		//update usuario
		user.User = d.User

		err = user.Update()
		if err != nil {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": "Error trying to update the nickname of User, try again."}
			JSONresponse(res, result)
			logger.ToEmail(err.Error()+"File users.go line 470", "Couldn't update User nickname via API")
			return
		}

		//obtaing IP
		ip, _, err := net.SplitHostPort(req.RemoteAddr)
		if err != nil {
			ip = "0.0.0.0"
			fmt.Println("userip: is not IP:port", req.RemoteAddr)
		}

		//saving log
		models.Log(user.ID, "updateNickName", "success", ip)

		//SUCCES user updated
		res.WriteHeader(http.StatusAccepted)
		//base 64 TOKEN
		result := map[string]string{"status": "success", "message": "User NickName updated succesfully", "id": user.ID.Hex()}
		JSONresponse(res, result)
		return

	}

	//forbidden
	res.WriteHeader(http.StatusForbidden)
	result := map[string]string{"status": "error", "message": "You don't have acces"}
	JSONresponse(res, result)
	return
}

//SetUserPwd reset pwd User
func SetUserPwd(res http.ResponseWriter, req *http.Request) {

	//get Headers
	apiKey := req.Header.Get("API-KEY")
	secretKey := req.Header.Get("SECRET-KEY")

	//validate headers
	_, access := models.ValidateProvider(apiKey, secretKey)

	//is a valid provider
	if access {

		//json data
		d := models.JSONUpdatePwd{}
		err := json.NewDecoder(req.Body).Decode(&d)
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": "Bad request, check Json Struct"}
			JSONresponse(res, result)
			return
		}

		//Validate JSON fields
		err = d.Validate()
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": err.Error()}
			JSONresponse(res, result)
			return
		}

		//user validation TODO VALIDATE STRING BEFORE APPLY BSON ***//
		user, error := models.GetUserById(bson.ObjectIdHex(d.ID))
		if error != nil {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": "Error to get user, invalid ID."}
			JSONresponse(res, result)
			return
		}

		//update PWD
		err = user.UpdatePassword(d.Pwd)
		if err != nil {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": "Error trying to update the User Password, try again."}
			JSONresponse(res, result)
			logger.ToEmail(err.Error()+"File users.go line 548", "Couldn't update User Password via API")
			return
		}

		//obtaing IP
		ip, _, err := net.SplitHostPort(req.RemoteAddr)
		if err != nil {
			ip = "0.0.0.0"
			fmt.Println("userip: is not IP:port", req.RemoteAddr)
		}

		//saving log
		models.Log(user.ID, "updatePwd", "success", ip)

		//SUCCES user updated
		res.WriteHeader(http.StatusAccepted)
		//base 64 TOKEN
		result := map[string]string{"status": "success", "message": "User Password updated succesfully", "id": user.ID.Hex()}
		JSONresponse(res, result)
		return

	}

	//forbidden
	res.WriteHeader(http.StatusForbidden)
	result := map[string]string{"status": "error", "message": "You don't have acces"}
	JSONresponse(res, result)
	return
}

//Login returns cookies session
func Login(res http.ResponseWriter, req *http.Request) {

	//get Headers
	apiKey := req.Header.Get("API-KEY")
	secretKey := req.Header.Get("SECRET-KEY")

	//validate headers
	_, access := models.ValidateProvider(apiKey, secretKey)

	//is a valid provider
	if access {

		//json data
		d := models.JSONLogin{}
		err := json.NewDecoder(req.Body).Decode(&d)
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": "Bad request, check Json Struct"}
			JSONresponse(res, result)
			return
		}

		//Validate JSON fields
		err = d.Validate()
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": err.Error()}
			JSONresponse(res, result)
			return
		}

		ip, _, err := net.SplitHostPort(req.RemoteAddr)
		if err != nil {
			ip = "0.0.0.0"
			fmt.Println("userip: is not IP:port", req.RemoteAddr)
		}

		user, err := models.Login(d.Email, d.Pwd)
		if err != nil {
			if user != nil {
				models.Log(user.ID, "login", "failed", ip)
			}
			result := map[string]string{"status": "error", "message": "no-match-account"}
			JSONresponse(res, result)
			return
		}

		// Set new session on DB and Set cookie
		models.NewSession(res, req, user)
		models.Log(user.ID, "login", "success", ip)

		//SUCCES user updated
		res.WriteHeader(http.StatusAccepted)
		//base 64 TOKEN
		result := map[string]string{"status": "success", "message": "User Login succesfully", "id": user.ID.Hex()}
		JSONresponse(res, result)
		return

	}

	//forbidden
	res.WriteHeader(http.StatusForbidden)
	result := map[string]string{"status": "error", "message": "You don't have acces"}
	JSONresponse(res, result)
	return
}

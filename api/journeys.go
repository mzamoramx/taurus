package api

import (

	/* native */

	"encoding/json"
	"fmt"
	"net"
	"net/http"

	/* addons */

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	/* home */

	"bitbucket.org/neuboxspacelab/taurus/logger"
	"bitbucket.org/neuboxspacelab/taurus/models"
)

// JourneyGetAll journeys
func JourneyGetAll(res http.ResponseWriter, req *http.Request) {

	//get Headers
	apiKey := req.Header.Get("API-KEY")
	secretKey := req.Header.Get("SECRET-KEY")

	//validate headers
	_, access := models.ValidateProvider(apiKey, secretKey)

	//is a valid provider
	if access {

		//json data
		d := models.GetJourneysCollection{}
		err := json.NewDecoder(req.Body).Decode(&d)
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": "Bad request, check Json Struct"}
			JSONresponse(res, result)
			return
		}

		//Validate JSON fields
		err = d.Validate()
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": err.Error()}
			JSONresponse(res, result)
			return
		}

		//get Journey model, all journeys!
		c, err := models.ListFullJourneys()
		if err != nil {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": err.Error()}
			JSONresponse(res, result)
			return
		}

		//parse into JSON
		journeys, err := json.Marshal(c)
		if err != nil {
			res.WriteHeader(http.StatusAccepted)
			result := map[string]string{"status": "success", "message": "Couldn't parse journeys data"}
			JSONresponse(res, result)
			return
		}

		//success!!
		res.WriteHeader(http.StatusAccepted)
		result := map[string]string{"status": "succes", "data": string(journeys), "message": "Data journeys served ::YouROCK::"}
		JSONresponse(res, result)
		return

	}

	//forbidden
	res.WriteHeader(http.StatusForbidden)
	result := map[string]string{"status": "error", "message": "You don't have access"}
	JSONresponse(res, result)
	return
}

//JourneyCreate a New Journey
func JourneyCreate(res http.ResponseWriter, req *http.Request) {

	//get Headers
	apiKey := req.Header.Get("API-KEY")
	secretKey := req.Header.Get("SECRET-KEY")

	//validate headers
	_, access := models.ValidateProvider(apiKey, secretKey)

	//is a valid provider
	if access {

		//json data
		d := models.JSONJourney{}
		err := json.NewDecoder(req.Body).Decode(&d)
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": "Bad request, check Json Struct"}
			JSONresponse(res, result)
			return
		}

		//Validate JSON fields
		err = d.Validate()
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": err.Error()}
			JSONresponse(res, result)
			return
		}

		//journey validation
		if exists, err := models.JourneyExists(d.Event); err != nil {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": "Error to review Event journey, try again."}
			JSONresponse(res, result)
			return
		} else if exists {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": "Journey Event already exists, we can't create the Journey :("}
			JSONresponse(res, result)
			return
		}

		//TODO validate objectidHex league
		league, error := models.GetLeagueByID(bson.ObjectIdHex(d.LeagueID))
		if error != nil {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": "Not league relationed, invalid ID."}
			JSONresponse(res, result)
			return
		}

		//Creating journey *****
		journey := &models.Journey{
			ID:       bson.NewObjectId(),
			Event:    d.Event,
			LeagueID: league.ID,
			Start:    d.Start,
			End:      d.End,
			Cron:     d.Cron,
			Status:   d.Status,
		}

		err = journey.Create()
		if err != nil {
			if mgo.IsDup(err) {
				res.WriteHeader(http.StatusConflict)
				result := map[string]string{"status": "error", "message": "Journey already created in DB"}
				JSONresponse(res, result)
				return
			}
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": "Error trying to create the journey, try again."}
			JSONresponse(res, result)
			logger.ToEmail(err.Error()+"File journey.go line 150", "Couldn't create journey via API")
			return
		}

		//obtaing IP
		ip, _, err := net.SplitHostPort(req.RemoteAddr)
		if err != nil {
			ip = "0.0.0.0"
			fmt.Println("userip: is not IP:port", req.RemoteAddr)
		}

		//saving log
		models.Log(journey.ID, "journeyCreated", "success", ip)

		//SUCCES journey created
		res.WriteHeader(http.StatusCreated)
		result := map[string]string{"status": "success", "message": "Journey created succesfully", "id": journey.ID.Hex()}
		JSONresponse(res, result)
		return

	}

	//forbidden
	res.WriteHeader(http.StatusForbidden)
	result := map[string]string{"status": "error", "message": "You don't have acces"}
	JSONresponse(res, result)
	return
}

//JourneyUpdate data journey
func JourneyUpdate(res http.ResponseWriter, req *http.Request) {

	//get Headers
	apiKey := req.Header.Get("API-KEY")
	secretKey := req.Header.Get("SECRET-KEY")

	//validate headers
	_, access := models.ValidateProvider(apiKey, secretKey)

	//is a valid provider
	if access {

		//json data
		d := models.JSONJourneyUpdate{}
		err := json.NewDecoder(req.Body).Decode(&d)
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": "Bad request, check Json Struct"}
			JSONresponse(res, result)
			return
		}

		//Validate JSON fields
		err = d.Validate()
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": err.Error()}
			JSONresponse(res, result)
			return
		}

		//journey validation TODO VALIDATE STRING BEFORE APPLY BSON *** //
		journey, error := models.GetJourneyByID(bson.ObjectIdHex(d.ID))

		if error != nil {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": "Error to get journey, invalid ID."}
			JSONresponse(res, result)
			return
		}

		//TODO validate objectidHex league
		league, error := models.GetLeagueByID(bson.ObjectIdHex(d.LeagueID))
		if error != nil {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": "Not league relationed, invalid ID."}
			JSONresponse(res, result)
			return
		}

		//Updating data journey *****
		journey.Event = d.Event
		journey.LeagueID = league.ID
		journey.Start = d.Start
		journey.End = d.End
		journey.Cron = d.Cron
		journey.Status = d.Status

		err = journey.Update()
		if err != nil {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": "Error trying to update the journey, try again."}
			JSONresponse(res, result)
			logger.ToEmail(err.Error()+"File journeys.go line 240", "Couldn't update journey via API")
			return
		}

		//obtaing IP
		ip, _, err := net.SplitHostPort(req.RemoteAddr)
		if err != nil {
			ip = "0.0.0.0"
			fmt.Println("userip: is not IP:port", req.RemoteAddr)
		}

		//saving log
		models.Log(journey.ID, "updateJourney", "success", ip)

		//SUCCES journey updated
		res.WriteHeader(http.StatusAccepted)
		//base 64 TOKEN
		result := map[string]string{"status": "success", "message": "journey updated succesfully", "id": journey.ID.Hex()}
		JSONresponse(res, result)
		return

	}

	//forbidden
	res.WriteHeader(http.StatusForbidden)
	result := map[string]string{"status": "error", "message": "You don't have acces"}
	JSONresponse(res, result)
	return
}

//JourneyGet data leajourneygue
func JourneyGet(res http.ResponseWriter, req *http.Request) {

	//get Headers
	apiKey := req.Header.Get("API-KEY")
	secretKey := req.Header.Get("SECRET-KEY")

	//validate headers
	_, access := models.ValidateProvider(apiKey, secretKey)

	//is a valid provider
	if access {

		//json data
		d := models.JSONGetJourney{}
		err := json.NewDecoder(req.Body).Decode(&d)
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": "Bad request, check Json Struct"}
			JSONresponse(res, result)
			return
		}

		//Validate JSON fields
		err = d.Validate()
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": err.Error()}
			JSONresponse(res, result)
			return
		}

		//journey validation TODO VALIDATE STRING BEFORE APPLY BSON *** //
		journey, error := models.GetJourneyByID(bson.ObjectIdHex(d.ID))

		if error != nil {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": "Error to get journey, invalid ID."}
			JSONresponse(res, result)
			return
		}

		//parse into JSON
		j, err := json.Marshal(journey)
		if err != nil {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "success", "message": "Error to parsed journey"}
			JSONresponse(res, result)
			return
		}

		//success!!
		res.WriteHeader(http.StatusAccepted)
		result := map[string]string{"status": "succes", "data": string(j), "message": "Journey served ::YouROCK::"}
		JSONresponse(res, result)
		return

	}

	//forbidden
	res.WriteHeader(http.StatusForbidden)
	result := map[string]string{"status": "error", "message": "You don't have acces"}
	JSONresponse(res, result)
	return
}

package api

import (
	"encoding/json"
	"fmt"
	"net/http"
)

//constants
const (
	DOMAIN  = "localhost/"
	STORAGE = "/go/src/bitbucket.org/neuboxspacelab/taurus/storage/" // Storage
)

// JSONresponse recieves a map and marshal it into json format, then send it to the user.
func JSONresponse(res http.ResponseWriter, response interface{}) {
	json, _ := json.Marshal(response)
	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(200)
	fmt.Fprintf(res, "%s", json)
}

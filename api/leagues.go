package api

import (

	/* native */

	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"os"

	/* addons */

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	/* home */

	"bitbucket.org/neuboxspacelab/taurus/logger"
	"bitbucket.org/neuboxspacelab/taurus/models"
	"bitbucket.org/neuboxspacelab/taurus/utils"
)

// LeagueGetAll leagues
func LeagueGetAll(res http.ResponseWriter, req *http.Request) {

	//get Headers
	apiKey := req.Header.Get("API-KEY")
	secretKey := req.Header.Get("SECRET-KEY")

	//validate headers
	_, access := models.ValidateProvider(apiKey, secretKey)

	//is a valid provider
	if access {

		//json data
		d := models.GetLeaguesCollection{}
		err := json.NewDecoder(req.Body).Decode(&d)
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": "Bad request, check Json Struct"}
			JSONresponse(res, result)
			return
		}

		//Validate JSON fields
		err = d.Validate()
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": err.Error()}
			JSONresponse(res, result)
			return
		}

		//get U model, all leagues!
		c, err := models.ListFullLeagues()
		if err != nil {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": err.Error()}
			JSONresponse(res, result)
			return
		}

		//parse into JSON
		leagues, err := json.Marshal(c)
		if err != nil {
			res.WriteHeader(http.StatusAccepted)
			result := map[string]string{"status": "success", "message": "Couldn't parse leagues data"}
			JSONresponse(res, result)
			return
		}

		//success!!
		res.WriteHeader(http.StatusAccepted)
		result := map[string]string{"status": "succes", "data": string(leagues), "message": "Data Leagues served ::YouROCK::"}
		JSONresponse(res, result)
		return

	}

	//forbidden
	res.WriteHeader(http.StatusForbidden)
	result := map[string]string{"status": "error", "message": "You don't have access"}
	JSONresponse(res, result)
	return
}

//LeagueCreate a New League
func LeagueCreate(res http.ResponseWriter, req *http.Request) {

	//get Headers
	apiKey := req.Header.Get("API-KEY")
	secretKey := req.Header.Get("SECRET-KEY")

	//validate headers
	_, access := models.ValidateProvider(apiKey, secretKey)

	//is a valid provider
	if access {

		//json data
		d := models.JSONLeague{}
		err := json.NewDecoder(req.Body).Decode(&d)
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": "Bad request, check Json Struct"}
			JSONresponse(res, result)
			return
		}

		//Validate JSON fields
		err = d.Validate()
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": err.Error()}
			JSONresponse(res, result)
			return
		}

		//Validate Countries JSON fields
		for _, element := range d.Countries {
			err = element.Validate()
			if err != nil {
				//badRequest json
				res.WriteHeader(http.StatusBadRequest)
				result := map[string]string{"status": "error", "message": err.Error()}
				JSONresponse(res, result)
				return
			}
		}

		//league validation
		if exists, err := models.LeagueExists(d.Name); err != nil {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": "Error to review name league, try again."}
			JSONresponse(res, result)
			return
		} else if exists {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": "League Name already exists, we can't create the league :("}
			JSONresponse(res, result)
			return
		}

		//Creating League *****
		league := &models.League{
			ID:        bson.NewObjectId(),
			Storage:   bson.NewObjectId(),
			Name:      d.Name,
			Countries: d.Countries,
			Available: d.Available,
		}

		err = league.Create()
		if err != nil {
			if mgo.IsDup(err) {
				res.WriteHeader(http.StatusConflict)
				result := map[string]string{"status": "error", "message": "League already created in DB"}
				JSONresponse(res, result)
				return
			}
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": "Error trying to create the league, try again."}
			JSONresponse(res, result)
			logger.ToEmail(err.Error()+"File league.go line 159", "Couldn't create league via API")
			return
		}

		//obtaing IP
		ip, _, err := net.SplitHostPort(req.RemoteAddr)
		if err != nil {
			ip = "0.0.0.0"
			fmt.Println("userip: is not IP:port", req.RemoteAddr)
		}

		//saving log
		models.Log(league.ID, "created", "success", ip)

		//create league folder
		path := "leagues/" + league.Storage.Hex()
		if err := utils.MkDirIfNotExists(path); err != nil {
			destPath, errPath := os.Create(path)

			if errPath != nil {
				fmt.Println(errPath)
			} else {
				fmt.Println("Dir Storage created")
			}
			destPath.Close()
		}

		//SUCCES league created
		res.WriteHeader(http.StatusCreated)
		result := map[string]string{"status": "success", "message": "League created succesfully", "id": league.ID.Hex()}
		JSONresponse(res, result)
		return

	}

	//forbidden
	res.WriteHeader(http.StatusForbidden)
	result := map[string]string{"status": "error", "message": "You don't have acces"}
	JSONresponse(res, result)
	return
}

//LeagueUpdate data leagues
func LeagueUpdate(res http.ResponseWriter, req *http.Request) {

	//get Headers
	apiKey := req.Header.Get("API-KEY")
	secretKey := req.Header.Get("SECRET-KEY")

	//validate headers
	_, access := models.ValidateProvider(apiKey, secretKey)

	//is a valid provider
	if access {

		//json data
		d := models.JSONLeagueUpdate{}
		err := json.NewDecoder(req.Body).Decode(&d)
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": "Bad request, check Json Struct"}
			JSONresponse(res, result)
			return
		}

		//Validate JSON fields
		err = d.Validate()
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": err.Error()}
			JSONresponse(res, result)
			return
		}

		//Validate Countries JSON fields
		for _, element := range d.Countries {
			err = element.Validate()
			if err != nil {
				//badRequest json
				res.WriteHeader(http.StatusBadRequest)
				result := map[string]string{"status": "error", "message": err.Error()}
				JSONresponse(res, result)
				return
			}
		}

		//league validation TODO VALIDATE STRING BEFORE APPLY BSON *** //
		league, error := models.GetLeagueByID(bson.ObjectIdHex(d.ID))

		if error != nil {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": "Error to get league, invalid ID."}
			JSONresponse(res, result)
			return
		}

		//Updating data league *****
		league.Name = d.Name
		league.Countries = d.Countries

		err = league.Update()
		if err != nil {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": "Error trying to update the league, try again."}
			JSONresponse(res, result)
			logger.ToEmail(err.Error()+"File leagues.go line 273", "Couldn't update league via API")
			return
		}

		//obtaing IP
		ip, _, err := net.SplitHostPort(req.RemoteAddr)
		if err != nil {
			ip = "0.0.0.0"
			fmt.Println("userip: is not IP:port", req.RemoteAddr)
		}

		//saving log
		models.Log(league.ID, "updateLeague", "success", ip)

		//SUCCES league updated
		res.WriteHeader(http.StatusAccepted)
		//base 64 TOKEN
		result := map[string]string{"status": "success", "message": "League updated succesfully", "id": league.ID.Hex()}
		JSONresponse(res, result)
		return

	}

	//forbidden
	res.WriteHeader(http.StatusForbidden)
	result := map[string]string{"status": "error", "message": "You don't have acces"}
	JSONresponse(res, result)
	return
}

//LeagueGet data league
func LeagueGet(res http.ResponseWriter, req *http.Request) {

	//get Headers
	apiKey := req.Header.Get("API-KEY")
	secretKey := req.Header.Get("SECRET-KEY")

	//validate headers
	_, access := models.ValidateProvider(apiKey, secretKey)

	//is a valid provider
	if access {

		//json data
		d := models.JSONGetLeague{}
		err := json.NewDecoder(req.Body).Decode(&d)
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": "Bad request, check Json Struct"}
			JSONresponse(res, result)
			return
		}

		//Validate JSON fields
		err = d.Validate()
		if err != nil {
			//badRequest json
			res.WriteHeader(http.StatusBadRequest)
			result := map[string]string{"status": "error", "message": err.Error()}
			JSONresponse(res, result)
			return
		}

		//league validation TODO VALIDATE STRING BEFORE APPLY BSON *** //
		league, error := models.GetLeagueByID(bson.ObjectIdHex(d.ID))

		if error != nil {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "error", "message": "Error to get league, invalid ID."}
			JSONresponse(res, result)
			return
		}

		//parse into JSON
		l, err := json.Marshal(league)
		if err != nil {
			res.WriteHeader(http.StatusConflict)
			result := map[string]string{"status": "success", "message": "Error to parsed league"}
			JSONresponse(res, result)
			return
		}

		//success!!
		res.WriteHeader(http.StatusAccepted)
		result := map[string]string{"status": "succes", "data": string(l), "message": "League served ::YouROCK::"}
		JSONresponse(res, result)
		return

	}

	//forbidden
	res.WriteHeader(http.StatusForbidden)
	result := map[string]string{"status": "error", "message": "You don't have acces"}
	JSONresponse(res, result)
	return
}
